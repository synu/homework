class JsonProductsListService < ProductsListService
  def parse_data(data)
    JSON.parse(data).map do |h|
      @products_list.push(
        Product.new(h['title'], h['price'].to_f, h['full_url'])
      )
    end
  end
end
