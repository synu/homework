class Product
  attr_accessor :price, :title, :url

  def initialize(title, price, url)
    @title = title
    @price = price
    @url = url
  end
end
