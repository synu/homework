class VarnishLogAnalyzerService
  def initialize(file_path)
    @log_file = File.open(file_path, 'r')
    @hostnames_requests = {}
    @files_requests = {}
  end

  def perform
    @log_file.each do |log|
      write_stats(log.match(/\"[A-Z]+ (?<file>\S+)? /)[:file])
    end

    print_results
  end

  private

  def write_stats(file_url)
    if @files_requests[file_url]
      @files_requests[file_url] += 1
    else
      @files_requests[file_url] = 1
    end

    if @hostnames_requests[hostname(file_url)]
      @hostnames_requests[hostname(file_url)] += 1
    else
      @hostnames_requests[hostname(file_url)] = 1
    end
  end

  def hostname(file_url)
    URI.parse(file_url).hostname
  end

  def print_results
    puts '5 most requested files:'
    puts @files_requests.sort_by { |_, v| v }.map(&:first).reverse[0..5]
    puts "\n5 most visited hostnames:"
    puts @hostnames_requests.sort_by { |_, v| v }.map(&:first).reverse[0..5]
  end
end
