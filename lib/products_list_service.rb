class ProductsListService
  def initialize(url)
    @products_list = []
    parse_data(open(url).read)
  end

  def perform
    @products_list.sort_by(&:price).each do |product|
      puts ProductFormatter.new(product)
    end
  end
end
