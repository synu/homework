class ProductFormatter
  def initialize(product)
    @product = product
  end

  def to_s
    "-------------------\n" \
      "#{@product.title}\n" \
      "#{@product.price}\n" \
      "#{@product.url}"
  end
end
