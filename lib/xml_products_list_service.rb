class XmlProductsListService < ProductsListService
  def parse_data(data)
    Hash.from_xml(data)['producten']['product'].map do |h|
      @products_list.push(
        Product.new(h['Titel'], h['Prijs'].tr(',', '.').to_f, h['Product_URL'])
      )
    end
  end
end
