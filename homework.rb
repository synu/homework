VARNISH_LOG_PATH = 'varnish.log'.freeze
XML_URL = 'https://my.datafeedwatch.com/static/files/4432/4331b414a3ff8bcf73968203997d061f2b311900.xml'.freeze
JSON_URL = 'ftp://dfw_test:RidetKoj0@ftp.datafeedwatch.com/feed.json'.freeze

require 'json'
require 'open-uri'
require 'active_support/core_ext/hash/conversions'

load 'lib/product.rb'
load 'lib/product_formatter.rb'
load 'lib/products_list_service.rb'
load 'lib/xml_products_list_service.rb'
load 'lib/json_products_list_service.rb'
load 'lib/varnish_log_analyzer_service.rb'

VarnishLogAnalyzerService.new(VARNISH_LOG_PATH).perform
XmlProductsListService.new(XML_URL).perform
JsonProductsListService.new(JSON_URL).perform
